# melvor-speed

Speed-running extension for Melvor Idle for FireFox and Chrome.

![Timer screenshot](screenshots/timer.png)

---

![Settings Screenshot](screenshots/settings.png)

## Development

1. If using FireFox, open about:debugging https://developer.mozilla.org/en-US/docs/Tools/about:debugging If using Chrome, open chrome://extensions/ and enable developer mode.
2. Load the extension on that page by selecting the "melvor-speed" folder
3. Open Melvor Idle and select a character

To reload, just click the "reload" button on about:debugging or chrome://extensions/. That should work for most changes, but if you edit the injection code, you may have to hard-reload. May also have to open dev tools and clear LocalStorage entries for "melvor-speed-*".

Or just run `npm run dev`, which sets up auto-reloading.

## Packaging

```bash
npm run build
```

## Acknowledgements

Thanks to Aldous Watts, author of MICE and SEMI, whose code I used to inject scripts into Melvor Idle.

Also thanks to @shenef who provided testing, feedback, and submitted the speedrun.com application for Melvor Idle.
