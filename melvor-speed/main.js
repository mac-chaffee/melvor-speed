// This logic taken from MICE: https://gitlab.com/aldousWatts/MICE/-/tree/main
// License: GNU GPLv3: https://gitlab.com/aldousWatts/MICE/-/blob/main/LICENSE
(() => {
    const isChrome = navigator.userAgent.match('Chrome');
    /* eslint-disable no-undef */
    const getSrc = (file) => (isChrome ? chrome : browser).runtime.getURL(file);
    const scriptID = 'melvor-speed-script';
    if (document.contains(document.getElementById(scriptID))) {
        document.getElementById(scriptID).remove();
    }
    const script = document.createElement('script');
    script.src = getSrc('melvor-speed.js');
    script.id = scriptID;
    document.body.appendChild(script);

    const settingsPageID = 'melvor-speed-settings-script';
    if (document.contains(document.getElementById(settingsPageID))) {
        document.getElementById(settingsPageID).remove();
    }
    const settingsPage = document.createElement('script');
    settingsPage.src = getSrc('melvor-speed-settings.js');
    settingsPage.id = settingsPageID;
    document.body.appendChild(settingsPage);
})();
