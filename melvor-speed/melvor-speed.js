/* eslint-disable no-undef */
MelvorSpeed = (() => {
    // Keys used for LocalStorage
    const START_TIME_KEY = 'melvor-speed-start-time';
    const END_TIME_KEY = 'melvor-speed-end-time';
    // const SPLIT_KEY_PREFIX = 'melvor-speed-split-';
    // Stores the element where the timer value is displayed. Global var for performance.
    let TIMER_VALUE_ELEM;
    // ID used for the div holding the cheats (used for disabling)
    const CHEATS_ID = 'melvor-speed-cheats';
    // Store the vanilla completeTutorial function to avoid recursion. Must assign at setup time since the function doesn't exist on character select screen.
    let oldCompleteTutorial;

    // Converts a number of milliseconds to "00:00:00.000" format
    const formatCurrentTime = (currentTimeMS) => {
        const hours = Math.floor(currentTimeMS / 1000 / 60 / 60);
        let remainingTime = currentTimeMS - (hours * 1000 * 60 * 60);
        const minutes = Math.floor(remainingTime / 1000 / 60);
        remainingTime -= (minutes * 1000 * 60);
        const seconds = Math.floor(remainingTime / 1000);
        const ms = remainingTime - (seconds * 1000);
        return `${String(hours).padStart(2, '0')}:${String(minutes).padStart(2, '0')}:${String(seconds).padStart(2, '0')}.${String(ms).padStart(3, '0')}`;
    };

    // This function is called via a VERY short setInterval to keep the time value accurate
    const updateTimer = async () => {
        // Don't update the time if the timer isn't running
        if (localStorage.getItem(START_TIME_KEY) === null) {
            return;
        }
        let currentTime;
        if (localStorage.getItem(END_TIME_KEY) !== null) {
            currentTime = parseInt(localStorage.getItem(END_TIME_KEY)) - parseInt(localStorage.getItem(START_TIME_KEY));
            TIMER_VALUE_ELEM.textContent = formatCurrentTime(currentTime);
        } else {
            currentTime = Date.now() - parseInt(localStorage.getItem(START_TIME_KEY));
            TIMER_VALUE_ELEM.textContent = formatCurrentTime(currentTime);
        }
    };

    // Runs when clicking the start/stop/reset button
    const startStopReset = async () => {
        // Perform "Start" action
        if (localStorage.getItem(START_TIME_KEY) === null) {
            localStorage.setItem(START_TIME_KEY, Date.now());
            const button = document.getElementById('melvor-speed-startstop');
            button.textContent = 'Stop';
            button.className = 'btn btn-lg btn-warning m-1 border-1x border-dark';
        // Perform "Stop" action
        } else if (localStorage.getItem(END_TIME_KEY) === null) {
            localStorage.setItem(END_TIME_KEY, Date.now());
            const button = document.getElementById('melvor-speed-startstop');
            button.textContent = 'Reset';
            button.className = 'btn btn-lg btn-danger m-1 border-1x border-dark';
        // Perform "Reset" action
        } else {
            localStorage.removeItem(START_TIME_KEY);
            localStorage.removeItem(END_TIME_KEY);
            const button = document.getElementById('melvor-speed-startstop');
            button.textContent = 'Start';
            button.className = 'btn btn-lg btn-primary m-1 border-1x border-dark';
            document.getElementById('melvor-speed-timer-value').innerHTML = '00:00:00.000';
        }
    };

    // Advances time by the number of hours specified in the melvor-speed-advance-time-value field
    const advanceTime = async () => {
        const timeVal = document.getElementById('melvor-speed-advance-time-value');
        // testOfflineFunction doesn't grow crops, must edit the plantTime manually
        for (let i = 0; i < newFarmingAreas.length; i++) {
            const area = newFarmingAreas[i];
            for (let j = 0; j < area.patches.length; j++) {
                const patch = area.patches[j];
                if (patch.timePlanted !== 0) {
                    patch.timePlanted -= (timeVal.valueAsNumber * 60 * 60 * 1000);
                }
            }
        }
        checkReadyToHarvest();
        /* eslint-disable no-undef */
        testOfflineFunction(timeVal.valueAsNumber);
    };

    // Wraps all changePage invocations to determine when a Tutorial% run starts
    const changePageWrapper = (...args) => {
        if (localStorage.getItem(MelvorSpeedSettings.AUTOSPLIT_DISABLED_KEY) === null && localStorage.getItem(MelvorSpeedSettings.SPLIT_PRESET_KEY) === 'tutorial%') {
            // Page zero is the woodcutting page, the first tutorial island task
            if (args[0] === 0 && !tutorialComplete && localStorage.getItem(START_TIME_KEY) === null) {
                startStopReset();
            }
        }
        return changePage(...args);
    };
    // Wraps completeTutorial to know when runs start/stop
    const completeTutorialWrapper = (...args) => {
        if (localStorage.getItem(MelvorSpeedSettings.AUTOSPLIT_DISABLED_KEY) !== null) {
            return oldCompleteTutorial(...args);
        }
        // Tutorial% runs stop when the tutorial is complete
        if (localStorage.getItem(MelvorSpeedSettings.SPLIT_PRESET_KEY) === 'tutorial%') {
            if (localStorage.getItem(START_TIME_KEY) !== null && localStorage.getItem(END_TIME_KEY) === null) {
                startStopReset();
            }
        // Any% runs start when the tutorial is complete (skipped)
        } else if (localStorage.getItem(MelvorSpeedSettings.SPLIT_PRESET_KEY) === 'any%') {
            if (localStorage.getItem(START_TIME_KEY) === null) {
                startStopReset();
            }
        }
        return oldCompleteTutorial(...args);
    };

    // Perform setup as soon as the game loads (after the character select screen)
    const melvorSpeedSetup = async () => {
        if (document.getElementById('melvor-speed') !== null) {
            document.getElementById('melvor-speed').remove();
        }
        const timer = document.createElement('div');
        timer.id = 'melvor-speed';
        timer.className = 'd-flex justify-content-between content-header';
        // Determine what state to start the timer button in
        let buttonText;
        let buttonClass;
        if (localStorage.getItem(START_TIME_KEY) === null) {
            buttonText = 'Start';
            buttonClass = 'btn-primary';
        } else if (localStorage.getItem(END_TIME_KEY) === null) {
            buttonText = 'Stop';
            buttonClass = 'btn-warning';
        } else {
            buttonText = 'Reset';
            buttonClass = 'btn-danger';
        }
        timer.innerHTML = `
            <div id="${CHEATS_ID}" class="input-group">
              <button class="btn btn-sm btn-secondary border-1x border-dark" id="melvor-speed-advance-time-button" onClick="MelvorSpeed.advanceTime()">Advance Time (hours)</button>
              <input type="number" class="form-control" value="18" max="18" style="max-width: 100px" id="melvor-speed-advance-time-value" aria-label="Advance time (hours)" aria-describedby="melvor-speed-advance-time-button">
            </div>
            <span id="melvor-speed-timer-value" class="d-inline-flex align-items-center m-1 ps-1" style="font-family: monospace">00:00:00.000</span>
            <button id="melvor-speed-startstop" class="btn btn-sm ${buttonClass} m-1 border-1x border-dark" onclick="MelvorSpeed.startStopReset()">${buttonText}</button>
        `;

        // Place in the header bar to prevent extra scrolling
        const existingHeaderUtils = document.getElementById('header-theme').children[1];
        document.getElementById('header-theme').insertBefore(timer, existingHeaderUtils);
        TIMER_VALUE_ELEM = document.getElementById('melvor-speed-timer-value');
        // 16ms allows the updates to appear accurate on 60 fps video
        TIMER_INTERVAL_ID = setInterval(updateTimer, 16);
        // Override changePage to detect splits
        const allPageChangers = document.querySelectorAll('div[onclick*="changePage("], a[onclick*="changePage("]');
        allPageChangers.forEach((element) => {
            element.setAttribute('onclick', element.getAttribute('onclick').replace('changePage(', 'MelvorSpeed.changePageWrapper('));
        });
        // Override completeTutorial()
        oldCompleteTutorial = completeTutorial;
        completeTutorial = completeTutorialWrapper;
    };

    // This function taken from MICE: https://gitlab.com/aldousWatts/MICE/-/tree/main
    // License: GNU GPLv3: https://gitlab.com/aldousWatts/MICE/-/blob/main/LICENSE
    // This is essentially the entrypoint for the whole script after it gets injected into the page.
    let preLoader;
    const checkLoaded = async () => {
        const accountName = document.getElementById('account-name');

        if (!accountName) return;
        const isCharacterSelected = accountName.innerText.length > 0;
        if (!isCharacterSelected) return;

        clearInterval(preLoader);
        melvorSpeedSetup();
    };
    preLoader = setInterval(checkLoaded, 200);
    // End MICE code

    // Return all functions that need to be called via onClick or from other files
    return {
        startStopReset, advanceTime, CHEATS_ID, changePageWrapper, completeTutorialWrapper,
    };
})();
