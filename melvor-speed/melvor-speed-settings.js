/* eslint-disable no-undef */
MelvorSpeedSettings = (() => {
    const HTML = `
<div class="row row-deck">
  <div class="col-md-12">
    <div class="block block-rounded block-link-pop border-top border-settings border-4x">
      <div class="block-content">
        <h2 class="content-heading border-bottom mb-4 pb-2">
          <span>Melvor Speed Settings</span>
        </h2>
        <div class="row">
          <div class="col-md-12">
            <div class="mb-4">
              <div class="custom-control custom-switch custom-control-lg">
                <input type="checkbox" class="custom-control-input" id="settings-toggle-6001"
                  name="settings-toggle-6001" checked="" onchange="MelvorSpeedSettings.toggleCheats()">
                <label class="custom-control-label font-size-sm text-muted" for="settings-toggle-6001">
                  <span>Enable cheats</span><br><small>
                    <span>Enables or disables speedrunning cheats.
                    </span>
                  </small>
                </label>
              </div>
            </div>
            <div class="mb-4">
              <div class="custom-control custom-switch custom-control-lg">
                <input type="checkbox" class="custom-control-input" id="settings-toggle-6002"
                  name="settings-toggle-6002" checked="" onchange="MelvorSpeedSettings.toggleAutoSplit()">
                <label class="custom-control-label font-size-sm text-muted" for="settings-toggle-6002">
                  <span>Enable autosplit</span><br><small>
                    <span>Enables or disables auto-start and auto-stop. Only Tutorial Island runs are supported for now. Runs start when you go to the Woodcutting page. Runs end when Tutorial Island is complete.
                    </span>
                  </small>
                </label>
              </div>
            </div>
            <div class="form-inline mb-4">
              <div class="dropdown">
                <button type="button" class="btn btn-primary dropdown-toggle font-size-sm" id="settings-dropdown-6003" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Tutorial%</button>
                <div class="dropdown-menu" aria-labelledby="settings-dropdown-6003">
                  <a class="dropdown-item" onclick="MelvorSpeedSettings.changeSplits('tutorial%')" id="settings-dropdown-6003-option-0">Tutorial%</a>
                  <a class="dropdown-item" onclick="MelvorSpeedSettings.changeSplits('any%')" id="settings-dropdown-6003-option-1">Any%</a>
                </div>
              </div>
              <span class="font-size-sm text-muted ml-2">Split preset<br><small>Which split preset to use. Tutorial% will start/stop runs when the tutorial starts/stops. Any% will start the run after skipping tutorial island, but will not automatically stop the run.</small></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
    `;
    // changePage() will select the new page by appending "-container"
    const DIV_ID_PREFIX = 'melvor-speed-settings';
    const SIDEBAR_LINK_ID = 'melvor-speed-settings-sidebar';
    const DIV_ID = `${DIV_ID_PREFIX}-container`;
    // LocalStorage key for cheats toggle
    const CHEATS_DISABLED_KEY = 'melvor-speed-settings-cheats-disabled';
    // LocalStorage key for auto-splitting
    const AUTOSPLIT_DISABLED_KEY = 'melvor-speed-settings-autosplit-disabled';
    const SPLIT_PRESET_KEY = 'melvor-speed-settings-split-preset';
    // Existing pages are 0-32, so this high index should keep our page safe
    const PAGE_ID = 6001;

    const toggleCheats = async () => {
        const cheats = document.getElementById(MelvorSpeed.CHEATS_ID);
        const present = cheats.classList.toggle('d-none');
        if (present) {
            localStorage.setItem(CHEATS_DISABLED_KEY, '1');
        } else {
            localStorage.removeItem(CHEATS_DISABLED_KEY);
        }
    };

    const toggleAutoSplit = async () => {
        if (localStorage.getItem(AUTOSPLIT_DISABLED_KEY) !== null) {
            localStorage.removeItem(AUTOSPLIT_DISABLED_KEY);
        } else {
            localStorage.setItem(AUTOSPLIT_DISABLED_KEY, '1');
        }
    };

    const changeSplits = async (value) => {
        localStorage.setItem(SPLIT_PRESET_KEY, value);
        switch (value) {
        case 'tutorial%':
            $('#settings-dropdown-6003').text($('#settings-dropdown-6003-option-0').text());
            break;
        case 'any%':
            $('#settings-dropdown-6003').text($('#settings-dropdown-6003-option-1').text());
            break;
        default:
            break;
        }
    };

    // Perform setup as soon as the game loads (after the character select screen)
    const melvorSpeedSettingsSetup = async () => {
        // Remove old page if it exists
        if (document.getElementById(DIV_ID) !== null) {
            document.getElementById(DIV_ID).remove();
        }
        // Inject a new page into the HTML and PAGES
        const settingsPage = document.createElement('div');
        settingsPage.id = DIV_ID;
        settingsPage.innerHTML = HTML;
        // Melvor toggles pages by adding/removing d-none inside changePage()
        settingsPage.className = 'content d-none';
        // Unfortunately, the strID is used for determining the header color and photo, but we can't inject those. So we get an ugly header.
        PAGES[PAGE_ID] = {
            name: DIV_ID_PREFIX, media: 'assets/media/main/something.svg', isVisible: true, strID: DIV_ID_PREFIX,
        };
        document.getElementById('main-container').appendChild(settingsPage);
        // Inject a link to the page in the sidebar
        // Remove old link if it exists
        if (document.getElementById(SIDEBAR_LINK_ID) !== null) {
            document.getElementById(SIDEBAR_LINK_ID).remove();
        }
        const settingsPageLink = document.createElement('li');
        settingsPageLink.className = 'nav-main-item';
        settingsPageLink.id = SIDEBAR_LINK_ID;
        settingsPageLink.innerHTML = `
            <div class="nav-main-link nav-compact pointer-enabled" onclick="changePage(${PAGE_ID});">
              <img class="nav-img" src="https://cdn.melvor.net/core/v018/assets/media/main/settings_header.svg">
              <span class="nav-main-link-name page-nav-name-6002">[Melvor Speed] Settings</span>
            </div>
        `;
        // The 16th index may change over time, but there's enough buffer for it to land somewhere on the navbar even if it's not the exact right place
        const vanillaSettings = document.querySelector('ul.nav-main').children[16];
        document.querySelector('ul.nav-main').insertBefore(settingsPageLink, vanillaSettings);
        // Disable cheats if localstorage says so
        if (localStorage.getItem(CHEATS_DISABLED_KEY) !== null) {
            document.getElementById('settings-toggle-6001').removeAttribute('checked');
            await toggleCheats();
        }
        // Disable autosplit if localstorage says so
        if (localStorage.getItem(AUTOSPLIT_DISABLED_KEY) !== null) {
            document.getElementById('settings-toggle-6002').removeAttribute('checked');
        }
        // Set split preference based on localstorage
        if (localStorage.getItem(SPLIT_PRESET_KEY) === null) {
            localStorage.setItem(SPLIT_PRESET_KEY, 'tutorial%');
        }
        changeSplits(localStorage.getItem(SPLIT_PRESET_KEY));
    };

    // This function taken from MICE: https://gitlab.com/aldousWatts/MICE/-/tree/main
    // License: GNU GPLv3: https://gitlab.com/aldousWatts/MICE/-/blob/main/LICENSE
    // This is essentially the entrypoint for the whole script after it gets injected into the page.
    let preLoader;
    const checkLoaded = async () => {
        const accountName = document.getElementById('account-name');

        if (!accountName) return;
        const isCharacterSelected = accountName.innerText.length > 0;
        if (!isCharacterSelected) return;

        clearInterval(preLoader);
        melvorSpeedSettingsSetup();
    };
    // Slower interval than main melvor-speed.js so we apply later
    preLoader = setInterval(checkLoaded, 500);
    return {
        toggleCheats, toggleAutoSplit, AUTOSPLIT_DISABLED_KEY, changeSplits, SPLIT_PRESET_KEY,
    };
})();
